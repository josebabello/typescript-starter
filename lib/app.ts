import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import axios, { AxiosResponse } from 'axios';
import { resolve } from "path";
class App {

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    public app: express.Application;

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

    private getUsers(): Promise<AxiosResponse> {
        return new Promise((resolve, reject) => {
            axios.get('https://randomuser.me/api/?results=5')
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
            });
        })
    }

    private routes(): void {
        const router = express.Router();
       
        router.get('/', (req: Request, res: Response) => {
            this.getUsers().then((result) => {
                console.log(result.data.results);
                res.status(200).send(result.data.results);
            });
        });

        router.post('/', (req: Request, res: Response) => {
            const data = req.body;
            // query a database and save data
            res.status(200).send(data);
        });

        this.app.use('/', router)

    }

}

export default new App().app;