"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const axios_1 = require("axios");
class App {
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
    getUsers() {
        axios_1.default.get('https://randomuser.me/api/?results=50')
            .then(function (response) {
            return response.data.results;
        })
            .catch(function (error) {
            console.log(error);
        })
            .then(function () {
        });
    }
    routes() {
        const router = express.Router();
        const users = this.getUsers();
        console.log(users);
        router.get('/', (req, res) => {
            res.status(200).send({ "users": users });
        });
        router.post('/', (req, res) => {
            const data = req.body;
            // query a database and save data
            res.status(200).send(data);
        });
        this.app.use('/', router);
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map